import { zhCN_account } from './account';
import { zhCN_globalTips } from './global/tips';

const zh_CN = {
  ...zhCN_account,
  ...zhCN_globalTips
};

export default zh_CN;