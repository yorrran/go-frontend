import { enUS_account } from './account';
import { enUS_globalTips } from './global/tips';

const en_US = {
  ...enUS_account,
  ...enUS_globalTips,
};

export default en_US;