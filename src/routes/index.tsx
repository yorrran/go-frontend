import type { FC } from 'react';
import type { RouteObject } from 'react-router';

import { lazy } from 'react';
import { useRoutes } from 'react-router-dom';

import LayoutPage from '@/pages/Layout';
import LoginPage from '@/pages/Login';
import CandidatePage from '@/pages/Candidate'
import ApplicationPage from '@/pages/Application'
import InterviewPage from '@/pages/Interview'

const NotFound = lazy(() => import(/* webpackChunkName: "404'"*/ '@/pages/404'));

const routeList: RouteObject[] = [
  {
    path: '/login',
    element: <LoginPage />,
  },
  {
    path: '/',
    element: <LayoutPage />,
    children: [
      {
        path: 'candidate',
        element:<CandidatePage/>,
      },
      {
        path: 'application',
        element:<ApplicationPage/>,
      },{
        path:'interview',
        element:<InterviewPage/>
      }
    ],
  },
];

const RenderRouter: FC = () => {
  const element = useRoutes(routeList);

  return element;
};

export default RenderRouter;
