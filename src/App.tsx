import './App.scss'
import { ConfigProvider } from 'antd';

import { history, HistoryRouter } from '@/routes/history';
import RenderRouter from './routes';

function App() {

  return (
    <>
      <ConfigProvider
      componentSize="middle"
    >
        <HistoryRouter history={history}>
            <RenderRouter />
        </HistoryRouter>
    </ConfigProvider>
    </>
  )
}

export default App
