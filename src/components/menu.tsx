import type { FC } from "react";
import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined
} from "@ant-design/icons";

import { Menu } from "antd";
import type { MenuProps } from "antd";
import { useNavigate } from "react-router-dom";

type MenuItem = Required<MenuProps>["items"][number];
type MenuLists = Array<MenuItem & { path: string }>;

const items: MenuLists = [
  {
    label: "Candidates",
    key: 0,
    icon: <MailOutlined />,
    path: "/candidate"
  },
  {
    label: "Applications",
    key: 1,
    icon: <AppstoreOutlined />,
    path: "/application"
  },
  {
    label: "Interviews",
    key: 2,
    icon: <SettingOutlined />,
    path: "/interview"
  }
];

const MenuComponent: FC<MenuProps> = (props) => {
  const navigate = useNavigate();

  const onMenuClick = (e: any) => {
    console.log("e:", e);
    const currItem = items[e.key]
    const path = currItem?.path
    navigate(path)
  };

  return (
    <Menu
      onClick={onMenuClick}
      style={{ width: 256, height:'100%' }}
      defaultSelectedKeys={["candidates"]}
      mode="inline"
      items={items}
    />
  );
};

export default MenuComponent;
