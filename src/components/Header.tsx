import type { FC } from 'react'
import '@/styles/header.scss'

const HeaderComponent: FC = (props) => {
    return <div className="header-wrapper"><div className="header-title">Candidate System</div></div>
}

export default HeaderComponent
