import type { FC } from 'react'

import '@/styles/layout.scss'

import { Layout } from 'antd'
import MenuComponent from '@/components/menu'
import { Suspense } from 'react'
import { Outlet } from 'react-router'
import HeaderComponent from '@/components/Header'
const WIDTH = 992
const { Sider, Content } = Layout

const LayoutPage: FC = () => {
    const toggle = () => {
        console.log('toggle')
    }
    return (
        <Layout className="layout-page">
            <HeaderComponent/>
            <Layout style={{display:"flex", flexDirection:"row"}}>
                <MenuComponent />
                <Content className="layout-page-content">
                    <Suspense fallback={null}>
                        <Outlet />
                    </Suspense>
                </Content>
            </Layout>
        </Layout>
    )
}

export default LayoutPage
