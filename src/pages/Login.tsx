import type { LoginParams } from '@/interfaces/user/login'
import type { FC } from 'react'

import '@/styles/login.scss'
import { Button, Form, Input } from 'antd'

const initialValues: LoginParams = {
    username: 'guest',
    password: 'guest',
    // remember: true
}

const LoginForm: FC = () => {
    const onFinished = async (form: LoginParams) => {
        console.log('login')
    }

    return (
        <div className="login-page">
            <div className="login-wrapper">
                <Form<LoginParams>
                    onFinish={onFinished}
                    className="login-page-form"
                    initialValues={initialValues}
                >
                    <h2>Candidates System</h2>
                    <Form.Item
                        name="username"
                        rules={[
                            {
                                required: true,
                                message: 'Please input correct user name',
                            },
                        ]}
                    >
                        <Input placeholder="Please input correct user name" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input correct password',
                            },
                        ]}
                    >
                        <Input
                            type="password"
                            placeholder="Please input correct password"
                        />
                    </Form.Item>
                    <div className="login-btns">
                        <Button
                            htmlType="submit"
                            type="primary"
                            className="login-page-form_button"
                        >
                            Login
                        </Button>
                        <Button
                            htmlType="submit"
                            type="primary"
                            className="login-page-form_button"
                        >
                            Register
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    )
}

export default LoginForm
